import { TMachinistQuestionType } from '../types';

/**
 * @interface IMachinistQuestionOptions Represents the defined options for a question.
 */
export interface IMachinistQuestionOptions {
  /**
   * @property Defines the type of the question.
   */
  type?: TMachinistQuestionType;

  /**
   * @property If false, the program will loop over the question while it doesn't get a valid and not-null answer.
   */
  nullable?: boolean;

  /**
   * @property Defines the default value of a question, if no answer (or an empty one) have been given. ignored if the question is not
   * nullable.
   */
  default?: any;

  /**
   * @property Only used if the question type is "enum". Defines the possible values of the answer.
   */
  values?: string[]|{ [key: string]: any }

  /**
   * @property Only used if the question type is "custom". Defines the pattern that the question answer must match with.
   */
  pattern?: RegExp;

  /**
   * @property Defines the message to display if the user answer to this question is not valid.
   */
  invalidAnswerMessage?: string;
}