/**
 * @interface IMachinistSettings Represents the settings applicable on Machinist instances.
 */
export interface IMachinistSettings {
  /**
   * @property Defines the message to display if the given answer was invalid.
   */
  invalidAnswerMessage?: string;
}