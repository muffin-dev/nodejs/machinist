import { IMachinistQuestionOptions } from './interfaces';
import { IMachinistSettings } from './interfaces';

/**
 * Contains the default question options.
 */
export const DEFAULT_QUESTION_OPTIONS: IMachinistQuestionOptions = {
  type: 'text',
  nullable: true,
  default: null,
  pattern: null,
  values: null,
  invalidAnswerMessage: null
};

/**
 * Contains the default settings for Machinist instances.
 */
export const DEFAULT_MACHINIST_SETTINGS: IMachinistSettings = {
  invalidAnswerMessage: null
};