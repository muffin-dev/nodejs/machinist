import { Interface, createInterface } from 'readline';
import { JSHelpers } from '@muffin-dev/js-helpers';

import { MachinistNode } from './machinist-node';
import { IMachinistQuestionOptions, IMachinistSettings } from '../interfaces';
import { TMachinistAction, TMachinistBlackboard, TMachinistCondition, TMachinistQuestionType, } from '../types';
import { DEFAULT_MACHINIST_SETTINGS } from '../constants';

export class Machinist {

  //#region Properties

  /**
   * @property Defines the settings for ths Machinist instance.
   */
  private _settings: IMachinistSettings = null;

  /**
   * @property Contains the root node of the Machinist process.
   */
  private _rootNode: MachinistNode = null;

  //#endregion

  //#region Accessors

  /**
   * Gets the settings used for this Machinist instance.
   */
  public get settings() {
    return this._settings;
  }

  //#endregion

  //#region Initialization

  /**
   * Creates an instance of Machinist.
   * @param settings The settings for ths Machinist instance.
   */
  constructor(settings: IMachinistSettings = null) {
    this._settings = Object.assign({ }, DEFAULT_MACHINIST_SETTINGS);
    if(settings !== null) {
      JSHelpers.overrideObject(this._settings, settings);
    }

    this._rootNode = new MachinistNode(this, null);
  }

  /**
   * Creates an instance of Machinist.
   * @param settings The settings to use for the new Machinist instance.
   */
  public static create(settings: IMachinistSettings = null) {
    return new Machinist(settings);
  }

  //#endregion

  //#region Public methods

  /**
   * Starts asking all the questions, and starts the process.
   * This method is a shortcut for calling ask() and exec() at the same time.
   * @param stream The I/O stream used for displaying questions to the user, and getting inputs. If null given, uses the standard I/O.
   * @param blackboard The blackboard object to use. If null given, uses an empty object.
   */
  public async run(stream: Interface = null, blackboard: TMachinistBlackboard = null) {
    blackboard = await this.ask(stream, blackboard);
    blackboard = await this.exec(blackboard);
    return blackboard;
  }

  /**
   * Starts asking all the questions.
   * @async
   * @param stream The I/O stream used for displaying questions to the user, and getting inputs. If null given, uses the standard I/O.
   * @param blackboard The blackboard object to use. If null given, uses an empty object.
   * @returns Returns the resulting blackboard.
   */
  public async ask(stream: Interface = null, blackboard: TMachinistBlackboard = null) {
    // Reset this machinist instance and the root node state
    this.reset();

    // Creates the default readline interface is the given onse is null
    if(stream === null) {
      stream = createInterface({
        input: process.stdin,
        output: process.stdout
      });
    }

    if(blackboard === null) {
      blackboard = { };
    }

    // Call ask() on each node
    await this._rootNode.ask(stream, blackboard, null);

    // Close the I/O stream
    stream.close();

    return blackboard;
  }

  /**
   * Executes all the node from the root node.
   * @param blackboard The blackboard object to use. If null given, uses an empty object.
   */
  public async exec(blackboard: TMachinistBlackboard = null) {
    if(blackboard === null) {
      blackboard = { };
    }

    await this._rootNode.exec(blackboard, null);
    return blackboard;
  }

  /**
   * Calls reset() on each node.
   */
  public reset() {
    this._rootNode.reset();
  }

  /**
   * Creates a new node that will contain a question that will be asked to the user.
   * @param question The text of the question to ask.
   * @param options The type or the options to use for this question.
   * @param blackboardKey The name of the blackboard variable that will contain the answer to this question.
   * @returns Returns the created node.
   */
  public question(question: string, options: TMachinistQuestionType|IMachinistQuestionOptions = null, blackboardKey: string = null) {
    return this._rootNode.question(question, options, blackboardKey);
  }

  /**
   * Adds an action to execute from the root node (so, without condition, and in the order the nodes are added).
   * This is useful for adding actions to perform before executing any other task, or after executing all of them.
   * @param action The action to execute.
   * @returns Returns the root node.
   */
  public do(action: TMachinistAction) {
    return this._rootNode.do(action);
  }

  /**
   * Creates a new node that will be executed only if the condition is fullfilled.
   * @param condition The method that will check for the condition.
   * @returns Returns the created node.
   */
  public if(condition: TMachinistCondition) {
    return this._rootNode.if(condition);
  }

  /**
   * Logs the nodes hierarchy.
   * @param showIds If true, shows the id number of each node in front of his name.
   */
  public logHierarchy(showIds = false) {
    this._rootNode.logHierarchy(0, showIds);
  }

  //#endregion

}