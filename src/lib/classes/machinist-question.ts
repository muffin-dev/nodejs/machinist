import { Interface } from 'readline';
import { IMachinistQuestionOptions } from '../interfaces';
import { TMachinistBlackboard, TMachinistQuestionType } from '../types';
import { Machinist } from './machinist';
import { JSHelpers } from '@muffin-dev/js-helpers';
import { DEFAULT_QUESTION_OPTIONS } from '../constants';
import { asBool } from '../utilities';

/**
 * @class Represents a question to ask to the user.
 */
export class MachinistQuestion {

  //#region Properties

  // References

  /**
   * @property The top-level Machinist instance.
   */
  private _machinist: Machinist = null;

  // Settings

  /**
   * @property The text of the question to ask.
   */
  private _question: string;

  /**
   * @property The options to use for this question.
   */
  private _options: IMachinistQuestionOptions = null;

  /**
   * @property The name of the blackboard variable that will contain this question's answer.
   */
  private _blackboardKey: string = null;

  // Cache

  /**
   * @property Stores the last answer of this question. Call reset() to reset this value.
   */
  private _answer: any = null;

  /**
   * Gets the answer to this question.
   */
  public get answer() {
    return this._answer;
  }

  /**
   * Gets the options of this question.
   */
  public get options() {
    return this._options;
  }

  //#endregion

  //#region Initialization

  constructor(
    machinist: Machinist,
    question: string,
    options: TMachinistQuestionType|IMachinistQuestionOptions = null,
    blackboardKey: string = null
  ) {
    this._machinist = machinist;
    this._question = this._prepareQuestionText(question);
    this._options = this._makeQuestionOptions(options);
    this._blackboardKey = blackboardKey;
  }

  //#endregion

  //#region Public methods

  /**
   * Asks for the question to the user, and process its answer depending on this question options.
   * @param stream The I/O interface to use for asking the question and getting user input.
   * @param blackboard The blackboard object to use. If this question has a defined blackboard key, its answer will be add to the
   * blackboard.
   */
  public async ask(stream: Interface, blackboard: TMachinistBlackboard) {
    let userAnswer: string = null;
    do {
      userAnswer = await this._askQuestion(this._question, stream);
    }
    while(!this._processAnswer(blackboard, userAnswer));

    return this._answer;
  }

  /**
   * Resets the answer to this question to null.
   */
  public reset() {
    this._answer = null;
  }

  //#endregion

  //#region Private Methods

  /**
   * Makes the given input option match with the expected question options structure, based on the default options.
   * @param inputOptions The question options to apply.
   */
  private _makeQuestionOptions(inputOptions: string|IMachinistQuestionOptions) {
    const options = Object.assign({ }, DEFAULT_QUESTION_OPTIONS);

    if(typeof inputOptions === 'string') {
      options.type = inputOptions as TMachinistQuestionType;
    }
    else {
      JSHelpers.overrideObject(options, inputOptions);
      options.type = options.type || 'text';
    }

    return options;
  }

  /**
   * Formats the question text.
   * @param question The original question text.
   */
  private _prepareQuestionText(question: string) {
    return question.trim() + ' ';
  }

  /**
   * Asks the given question to the user, using the given Readline Interface.
   * Basically an async version of readline.Interface.question().
   * @param question The question to ask to the user.
   * @param stream The I/O stream to use for asking that question, and read the answer.
   */
  private async _askQuestion(question: string, stream: Interface) {
    return new Promise((resolve: (data: any) => void) => {
      stream.question(question, value => {
        resolve(value);
      });
    });
  }

  /**
   * Checks if the given answer is valid for this question, and save it if it's ok.
   * @param blackboard The blackboard object from the top-level Machinist instance.
   * @param answer The user answer to the question.
   * @returns Returns true if the answer is valid, otherwise false.
   */
  private _processAnswer(blackboard: TMachinistBlackboard, answer: string) {
    // If the given answer is null or empty...
    if(answer === null || answer === undefined || answer === '') {
      // ... but the question is not nullable: show error and decline the answer
      if(!this._options.nullable) {
        this._showInvalidAnswerMessage();
        return false;
      }
      // ... but the question is nullable: use the default value as the answer
      else {
        answer = this._options.default;
        return true;
      }
    }

    let convertedAnswer: any = null;
    let isAnswerValid = false;

    // Convert the answer
    switch(this._options.type) {
      // Boolean conversion
      case 'boolean':
      case 'bool':
      {
        answer = answer.toLowerCase();
        convertedAnswer = asBool(answer);
        isAnswerValid = true;
      }
      break;

      // Integer conversion
      case 'integer':
      case 'int':
      {
        convertedAnswer = parseInt(answer, 10);
        isAnswerValid = !isNaN(convertedAnswer);
      }
      break;

      // Float conversion
      case 'number':
      {
        convertedAnswer = parseFloat(answer);
        isAnswerValid = !isNaN(convertedAnswer);
      }
      break;

      // Enum check
      case 'enum':
      {
        convertedAnswer = answer;
        let possibleValues: string[] = null;
        if(Array.isArray(this._options.values)) {
          possibleValues = this._options.values;
        }
        else if(typeof possibleValues === 'object') {
          possibleValues = Object.keys(this._options.values);
        }
        else {
          possibleValues = [];
        }

        for(const v of possibleValues) {
          if(v === answer) {
            isAnswerValid = true;
            break;
          }
        }
      }
      break;

      // Pattern match check
      case 'custom':
      {
        convertedAnswer = answer;
        isAnswerValid = answer.match(this._options.pattern) !== null;
      }
      break;

      // By default, consider the value is valid
      default:
      {
        convertedAnswer = answer;
        isAnswerValid = true;
      }
      break;
    }

    // Save the answer if it's valid
    if(isAnswerValid) {
      this._saveAnswer(blackboard, convertedAnswer);
    }
    // Log invalid answer message if the answer is not valid
    else {
      this._showInvalidAnswerMessage();
    }

    return isAnswerValid;
  }

  /**
   * Store the answer in class properties, and write it on the blackboard if a key is defined.
   * @param blackboard The blackboard object from the top-level Machinist instance.
   * @param answer The answer to save.
   */
  private _saveAnswer(blackboard: TMachinistBlackboard, answer: any) {
    this._answer = answer;
    if(this._blackboardKey) {
      blackboard[this._blackboardKey] = answer;
    }
  }

  /**
   * Write an invalid answer message using the given stream output.
   * Uses this question's invalid answer message defined in its options, or the Machinist's if it's not defined.
   */
  private _showInvalidAnswerMessage() {
    if(this._options.invalidAnswerMessage) {
      console.log(this._options.invalidAnswerMessage);
    }
    else if(this._machinist.settings.invalidAnswerMessage) {
      console.log(this._machinist.settings.invalidAnswerMessage);
    }
  }

  //#endregion

}