import { Interface } from 'readline';
import { Machinist } from './machinist';
import { TMachinistAction, TMachinistBlackboard, TMachinistCondition, TMachinistQuestionType } from '../types';
import { IMachinistQuestionOptions } from '../interfaces';
import { MachinistQuestion } from './machinist-question';

export class MachinistNode {

  //#region Properties

  // Define node ids for debug purposes
  private static _id = 0;

  /**
   * @property This node's id number.
   */
  private _nodeId = 0;

  /**
   * @property The top-level Machinist instance.
   */
  private _machinist: Machinist = null;

  /**
   * @property The parent node of this one.
   */
  private _parent: MachinistNode = null;

  /**
   * @property The clid nodes of this one.
   */
  private _children = new Array<MachinistNode>();

  /**
   * @property The question to ask to the user at the "ask" phase.
   */
  private _question: MachinistQuestion = null;

  /**
   * @property The actions to execute at the "exec" phase.
   */
  private _actions = new Array<TMachinistAction>();

  /**
   * @property The method that will check for a condition. If a condition is set, but that condition is not fullfilled, the question of
   * this node is ignored, and its actions are not executed.
   */
  private _condition: TMachinistCondition = null;

  /**
   * @property The action to call after this node's question have been asked, whatever its answer.
   */
  private _setup: TMachinistAction = null;

  /**
   * Adds 1 to the static id number, and returns the new number.
   */
  public static get nextId() {
    return MachinistNode._id++;
  }

  /**
   * Gets the parent node of this one.
   */
  public get parent() {
    return this._parent;
  }

  /**
   * Gets the parent node of this one.
   * Alias of parent().
   */
  public get p() {
    return this.parent;
  }

  /**
   * Goes up the hierarchy of this node to get the parent question node, and returns it.
   */
  public get parentQuestion() {
    let questionNode = this as MachinistNode;
    do {
      questionNode = questionNode.parent;
    }
    while (questionNode._question === null);
    return questionNode;
  }

  /**
   * Goes up the hierarchy of this node to get the last question node, and returns it.
   * Alias of parentQuestion().
   */
  public get pq() {
    return this.parentQuestion;
  }

  //#endregion

  //#region Initialization

  /**
   * Class constructor.
   * @param machinist The top-level Machinist instance.
   * @param parent The parent node of this one.
   */
  constructor(machinist: Machinist, parent: MachinistNode) {
    this._machinist = machinist;
    this._parent = parent;

    this._nodeId = MachinistNode.nextId;
  }

  //#endregion

  //#region Public methods

  /**
   * Goes up the hierarchy by the given level. For example, back(2) will gets the parent of the parent node of this one.
   * back(1) is equivalent to parent().
   * @param levels The number of parent nodes to iterate through.
   * @returns Returns the found node.
   */
  public back(levels: number) {
    let node = this as MachinistNode;
    for(let i = 0; i < levels; i++) {
      node = node.parent;
    }
    return node;
  }

  /**
   * Starts the "ask" phase for this node. It checks its condition, asks for the question to the user, and call ask() on the child nodes.
   * @async
   * @param stream The interface to use for asking the question and getting user input.
   * @param blackboard The blackboard object to use.
   * @param answer The answer to the parent question (null if the previous node doesn't contain a question).
   * @returns Returns the answer to this node's question, or the input answer if this node doesn't contain a question.
   */
  public async ask(stream: Interface, blackboard: TMachinistBlackboard, answer: any): Promise<any> {
    // Cancel operation if the condition is not fullfilled
    if(this._condition) {
      const canContinue = await this._condition(blackboard, answer);
      if(!canContinue) {
        return;
      }
    }

    // Asks the question of this node if there's one
    if(this._question !== null) {
      answer = await this._question.ask(stream, blackboard);
    }

    // Run "setup" method if there's one
    if(this._setup) {
      await this._setup(blackboard, answer);
    }

    // Call ask() on each child node
    for(const childNode of this._children) {
      await childNode.ask(stream, blackboard, answer);
    }

    return answer;
  }

  /**
   * Starts the "exec" phase for this node. It checks its condition, executes its actions, and call exec() on the child nodes.
   * @async
   * @param blackboard The blackboard object to use.
   * @param answer The answer to the parent question (null if the parent node doesn't contain a question).
   */
  public async exec(blackboard: TMachinistBlackboard, answer: any) {
    // Cancel operation if the condition is not fullfilled
    if(this._condition) {
      const canContinue = this._condition(blackboard, answer);
      if(!canContinue) {
        return;
      }
    }

    // If this node has a question, send its answer to child nodes
    if(this._question !== null) {
      answer = this._question.answer;
    }

    // Execute this node's actions
    for(const action of this._actions) {
      await action(blackboard, answer);
    }

    // Call exec() on each child node
    for(const childNode of this._children) {
      await childNode.exec(blackboard, answer);
    }
  }

  /**
   * Resets this node's state: reset the question answer to null, and call reset() on child nodes.
   */
  public reset() {
    if(this._question !== null) {
      this._question.reset();
    }

    for(const childNode of this._children) {
      childNode.reset();
    }
  }

  /**
   * Defines an action to execute just after this node's question have been asked, whatever its answer. Keep in mind that at this step,
   * all the questions haven't been asked, and the blackboard could be incomplete.
   * @param action The action to execute after this node's question have been asked.
   */
  public setup(action: TMachinistAction) {
    this._setup = action;
    return this;
  }

  /**
   * Creates a node that will be executed only if the given condition is fullfilled.
   * @param condition The method that will check for the condition.
   * @returns Returns the created node.
   */
  public if(condition: TMachinistCondition) {
    const newNode = new MachinistNode(this._machinist, this);
    newNode._condition = condition;
    this._children.push(newNode);
    return newNode;
  }

  /**
   * Creates a new node that will contain a question that will be asked to the user.
   * @param question The text of the question to ask.
   * @param options The type or the options to use for this question.
   * @param blackboardKey The name of the blackboard variable that will contain the answer to this question.
   * @returns Returns the created node.
   */
  public question(question: string, options?: TMachinistQuestionType|IMachinistQuestionOptions, blackboardKey?: string) {
    const newNode = new MachinistNode(this._machinist, this);
    newNode._question = new MachinistQuestion(this._machinist, question, options, blackboardKey);
    this._children.push(newNode);
    return newNode;
  }

  /**
   * Creates a new node that will be executed only if the answer to this node's question matches with the given expected answer.
   * @param expectedAnswer The expected answer to this node's question.
   * @returns Returns the created node.
   */
  public case(expectedAnswer: any) {
    const newNode = new MachinistNode(this._machinist, this);
    newNode._condition = (blackboard, answer) => {
      return answer === expectedAnswer;
    };
    this._children.push(newNode);
    return newNode;
  }

  /**
   * Adds an action to execute from this node.
   * @param action The action to execute.
   * @returns Returns itself.
   */
  public do(action: TMachinistAction) {
    this._actions.push(action);
    return this;
  }

  /**
   * Logs the nodes hierarchy.
   * @param depth The current hierarchy depth. If null given, starts at a depth of 0.
   * @param showIds If true, shows the id number of each node in front of his name.
   */
  public logHierarchy(depth: number = null, showIds = false) {
    if(!depth) {
      depth = 0;
    }

    const spaces = ' '.repeat(2);
    const tab = spaces.repeat(depth);

    // Define the name of this node
    let nodeName = '';

    if(this._condition) {
      nodeName = 'Conditional node';
    }
    if(this._question) {
      nodeName = (nodeName === '') ? 'Question node' : nodeName + ' - Question';
    }
    if(nodeName === '') {
      nodeName = this._parent === null ? 'Root node' : 'Empty node';
    }

    // Add node id number if required
    if(showIds) {
      nodeName = `[${this._nodeId}] ${nodeName}`;
    }

    let log = nodeName;
    if(this._actions.length > 0) {
      log += ` (${this._actions.length} actions)`;
    }

    console.log(tab + log);

    // Logs the hierarchy of the child nodes
    for(const childNode of this._children) {
      childNode.logHierarchy(depth + 1);
    }
  }

  //#endregion

}