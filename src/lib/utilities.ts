import { JSHelpers } from '@muffin-dev/js-helpers';

/**
 * Converts the given value into a boolean.
 * If the input value is a string, it's considered true if it's equal to: "t", "true", "y", "yes" or "1".
 * Else, use JSHelpers.asBoolean() method.
 * @param value The value to convert.
 */
export function asBool(value: any) {
  if(typeof value  === 'string') {
    return value === 't' || value === 'true' || value === 'y' || value === 'yes' || value === '1';
  }
  return JSHelpers.asBoolean(value);
}