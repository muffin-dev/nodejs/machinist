/**
 * @enum Defines the type of a question.
 */
export type TMachinistQuestionType =
  /**
   * Used by default. The answer can be anything, but will be converted into text.
   */
  'text' |

  /**
   * The answer can be one of several defined choices. These choices are set by the "values" question option.
   */
  'enum' |

  /**
   *  The answer must be a boolean. The answer is considered true when equal to "t", "true", "y", "yes" or "1".
   */
  'boolean' |

  /**
   *  Alias of boolean.
   */
  'bool' |

  /**
   *  The asnwer must be a number, integer or decimal.
   */
  'number' |

  /**
   *  The answer must be an integer number.
   */
  'integer' |

  /**
   *  Alias of integer.
   */
  'int' |

  /**
   *  The answer must match with a custom pattern, defined in "pattern" question option.
   */
  'custom';

/**
 * @type Represents the blackboard object.
 */
export type TMachinistBlackboard = { [key: string]: any };

/**
 * @type Represents an action to execute, generally for the "exec" phase.
 * @param blackboard The blackboard object used in the Machinist process.
 * @param answer The answer to the parent question (null if there's no parent question).
 */
export type TMachinistAction = (blackboard: TMachinistBlackboard, answer: any) => void | Promise<void>;

/**
 * @type Represents a method that checks for a condition.
 * @param blackboard The blackboard object used in the Machinist process.
 * @param answer The answer to the parent question (null if there's no parent question).
 * @returns Returns true if the condition is fullfilled, otherwise false.
 */
export type TMachinistCondition = (blackboard: TMachinistBlackboard, answer: any) => boolean | Promise<boolean>;