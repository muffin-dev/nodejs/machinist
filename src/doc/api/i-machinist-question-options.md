# Muffin Dev for Node - Machinist - `IMachinistQuestionOptions` interface

Represents the defined options for a question.

This interface is used for the `question()` method of [`Machinist`](./machinist.md) and [`MachinistNode`](./machinist-node.md) classes.

## Implementation

```ts
interface IMachinistQuestionOptions {
  // Defines the type of the question ("text" by default).
  type?: TMachinistQuestionType;

  // If false, the program will loop over the question while it doesn't get a valid and not-null answer (true by default).
  nullable?: boolean;

  // Defines the default value of a question, if no answer (or an empty one) have been given. ignored if the question is not (null by default).
  default?: any;

  // Only used if the question type is "enum". Defines the possible values of the answer.
  values?: string[]|{ [key: string]: any }

  // Only used if the question type is "custom". Defines the pattern that the question answer must match with.
  pattern?: RegExp;

  // Defines the message to display if the user answer to this question is not valid (null by default).
  invalidAnswerMessage?: string;
}
```

## Related links

- [`Machinist` class](./machinist.md)
- [`MachinistNode` class](./machinist-node.md)
- [`TMachinistQuestionType` type](./t-machinist-question-type.md)