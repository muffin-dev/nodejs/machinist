# Muffin Dev for Node - Machinist - `TMachinistBlackboard`

Represents the blackboard object.

This object is passed to each Machinist node, and is mainly used for storing the answers to all the questions. You can also store custom informations at the "exec" phase if you want.

## Implementation

```ts
type TMachinistBlackboard = { [key: string]: any };
```