# Muffin Dev for Node - Machinist - `TMachinistCondition` type

Represents a method that checks for a condition.

This is for example used in the `if()` method of [`Machinist`](./machinist.md) and [`MachinistNode`](./machinist-node.md) classes.

## Implementation

```ts
type TMachinistCondition = (blackboard: TMachinistBlackboard, answer: any) => boolean|Promise<boolean>;
```

- `blackboard: TMachinistBlackboard`: The blackboard object used in the Machinist process
- `answer: any`: The answer to the parent question (null if there's no parent question)

Returns `true` if the condition is fullfilled, otherwise `false`.

## Related links

- [`Machinist` class](./machinist.md)
- [`MachinistNode` class](./machinist-node.md)
- [`TMachinistBlackboard` type](./t-machinist-blackboard.md)