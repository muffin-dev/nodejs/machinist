# Muffin Dev for Node - Machinist - `TMachinistAction` type

Represents an action to execute, generally at the "exec" phase.

## Implementation

```ts
type TMachinistAction = (blackboard: TMachinistBlackboard, answer: any) => void|Promise<void>;
```

- `blackboard: TMachinistBlackboard`: The blackboard object used in the Machinist process
- `answer: any`: The answer to the parent question (null if there's no parent question)