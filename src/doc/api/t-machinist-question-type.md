# Muffin Dev for Node - Machinist - `TMachinistQuestionType` type

Defines the type of a question.

This is used in the `question()` method of [`Machinist`](./machinist.md) and [`MachinistNode`](./machinist-node.md) classes, and in the [`IMachinistQuestionOptions`](./i-machinist-question-options.md) interface.

## Implementation

```ts
type TMachinistQuestionType =
  // Used by default. The answer can be anything, but will be converted into text.
  'text'    |

  // The answer can be one of several defined choices. These choices are set by the "values" question option (see IMachinistQuestionOptions interface documentation).
  'enum'    |

  // The answer must be a boolean. The answer is considered true when equal to "t", "true", "y", "yes" or "1".
  'boolean' |

  // Alias of boolean.
  'bool'    |

  // The asnwer must be a number, integer or decimal.
  'number'  |

  // The answer must be an integer number.
  'integer' |

  // Alias of integer.
  'int'     |

  // The answer must match with a custom pattern, defined in "pattern" question option (see IMachinistQuestionOptions interface documentation).
  'custom';
```

## Related links

- [`Machinist` class](./machinist.md)
- [`MachinistNode` class](./machinist-node.md)
- [`IMachinistQuestionOptions` interface](./i-machinist-question-options.md)