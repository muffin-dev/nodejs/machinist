# Muffin Dev for Node - Machinist - `Machinist` class

The `Machinist` class is the top-level instance that can run the Machinist process.

## Basics

### Import

Import *Machinist* in vanilla JS:

```js
// Import Machinist
const machinist = require('@muffin-dev/machinist');

// Create an instance of Machinist
const myMachinist = machinist();
```

---

Import *Machinist* using TypeScript:

```ts
// Import Machinist
import Machinist from '@muffin-dev/machinist';

// Create an instance of Machinist
const myMachinist = Machinist();
```

### Usage

```js
// Add a question to ask:
myMachinist.question('What\'s your name?')
    // Add an action to perform
    .do((blackboard, answer) => { console.log(`Hello ${answer}!`); });

// Run your Machinist instance
myMachinist.run();
```

## Methods

### Process methods

```ts
async run(stream: readline.Interface = null, blackboard: TMachinistBlackboard = null): Promise<TMachinistBlackboard>
```

Shortcut for calling `ask()` and `exec()` methods in one call, which will respectively run the *ask* phase, and the *exec* phase.

- `stream: readline.Interface`: The I/O stream used for displaying questions to the user, and getting inputs. If null given, uses the standard I/O (`process.stdin` and `process.stdout`)
- `blackboard: TMachinistBlackboard`: The blackboard object to use. If null given, uses an empty object

Returns the blackboard object, after the both phases have been executed.

Also see:

- [`TMachinistBlackboard` type](./t-machinist-blackboard.md)

---

```ts
async ask(stream: readline.Interface = null, blackboard: TMachinistBlackboard = null): Promise<TMachinistBlackboard>
```

Starts the *ask* phase, which consists to asks the created questions to the user, and get its inputs. Note that you can use the `run()` method in order to run the complete process (the *ask* and the *exec* phase) in one call.

To be more precise, this method will call `ask()` on the root node of a `Machinist` instance, which will cause it to recursively call `ask()` on all its child nodes.

Also note that if you define a stream for this method, it's closed after calling `ask()` on all the nodes of the tree.

- `stream: readline.Interface`: The I/O stream used for displaying questions to the user, and getting inputs. If null given, uses the standard I/O (`process.stdin` and `process.stdout`)
- `blackboard: TMachinistBlackboard`: The blackboard object to use. If null given, creates an empty object

Returns the blackboard object, as it is after asking all the questions.

Also see:

- [`TMachinistBlackboard` type](./t-machinist-blackboard.md)

---

```ts
async exec(stream: readline.Interface = null, blackboard: TMachinistBlackboard = null): Promise<TMachinistBlackboard>
```

Starts the *exec* phase, which consists to executes all the actions created using the `do()` method from this class or from nodes (see [`MachinistNode` class](./machinist-node.md)). Note that you can use the `run()` method in order to run the complete process (the *ask* and the *exec* phase) in one call.

To be more precise, this method will call `exec()` on the root node of a `Machinist` instance, which will cause it to recursively call `exec()` on all its child nodes.

- `blackboard: TMachinistBlackboard`: The blackboard object to use. If null given, creates an empty object

Returns the blackboard object, as it is after executing all the actions.

Also see:

- [`MachinistNode` class](./t-machinist-node.md)
- [`TMachinistBlackboard` type](./t-machinist-blackboard.md)

---

```ts
reset(): void
```

Calls reset() on each node. Used internally when you call `ask()`, before the *ask* phase starts.

### Nodes methods

```ts
question(question: string, options: TMachinistQuestionType|IMachinistQuestionOptions = null, blackboardKey: string = null): MachinistNode
```

Creates a new node that will contain a question that will be asked to the user.

- `question: string`: The text of the question to ask
- `options?: TMachinistQuestionType|IMachinistQuestionOptions`: The type or the options to use for this question (see [`TMachinistQuestionType` type](./t-machinist-question-type.md) and [`IMachinistQuestionOptions` interface](./i-machinist-question-options.md))
- `blackboardKey: string`: The name of the blackboard variable that will contain the answer to this question. If null, the answer won't be stored in the blackboard

Returns the created node.

Example:

```ts
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Create a question with default options, which is text type
machinist.question('What\'s your name?', null, 'name');
// Create a question that will be asked again until the answer is a number
machinist.question('How old are you?', 'integer', 'age');
// Create a question that will be asked again until the answer is one of the defined values
machinist.question('What\'s your gender? (female|male|other)', { type: 'enum', values: ['female', 'male', 'other']}, 'gender');
// Log the blackboard, which contain all the answers at the defined keys, here: { name, age, gender }
machinist.do((blackboard) => { console.log('Output blackboard', blackboard); });
// Run Machinist
machinist.run();
```

Also see:

- [`MachinistNode` class](./t-machinist-node.md)
- [`IMachinistQuestionOptions` interface](./i-machinist-question-options.md)
- [`TMachinistQuestionType` type](./t-machinist-question-type.md)

---

```ts
do(action: TMachinistAction): MachinistNode
```

Creates an action to execute from the root node. That action won't be related to a question or a condition. This is useful to execute action that should be executed before or after the *exec* phase.

- `action: TMachinistAction`: The action to execute

Returns the root node.

Example:

```ts
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Creates an action that logs something, which will always appear, even if questions are defined before or after this line
machinist.do(() => { console.log('Notice Machinist process begins'); });
// Run Machinist
machinist.run();
```

Also see:

- [`MachinistNode` class](./t-machinist-node.md)
- [`TMachinistAction` type](./t-machinist-action.md)

---

```ts
if(condition: TMachinistCondition): MachinistNode
```

Creates a new node that will be executed only if the condition is fullfilled.

- `condition: TMachinistCondition`: The method that will check for the condition

Returns the created node.

Example:

```js
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Check if the blackboard.askName variable is valid
machinist.if((blackboard) => { return blackboard.askName; })
    // This question will be asked only if the condition is fullfilled
    .question('What\'s your name?', 'text', 'name');
// Run Machinist with a askName key set to true in the blackboard object
machinist.run(null, { askName: true });
```

Also see:

- [`MachinistNode` class](./t-machinist-node.md)
- [`TMachinistCondition` type](./t-machinist-condition.md)

### Utility methods

```ts
logHierarchy(showIds = false): void
```

Logs the nodes hierarchy, from the root node of a Machinist instance.

- `showIds: boolean`: If true, shows the id number of each node in front of his name