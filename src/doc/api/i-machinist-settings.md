# Muffin Dev for Node - Machinist - `IMachinistSettings` interface

Represents the settings applicable on Machinist top-level instances.

This interface is used for the [`Machinist.create()`](./machinist.md) method, which is the default export of the *Machinist* module.

## Implementation

```ts
interface IMachinistSettings {
  // Defines the message to display if the given answer was invalid (null by default).
  invalidAnswerMessage?: string;
}
```

## Related links

- [`Machinist` class](./machinist.md)