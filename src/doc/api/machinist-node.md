# Muffin Dev for Node - Machinist - `MachinistNode` class

See the *Machinist* structure as a tree. A node can contain a question to ask to the user, or a condition that applies to its child nodes. They can also contain some actions to execute during the *exec* phase.

This structure also means that you'll need to navigate through these nodes. See *Navigation methods* section to learn more about it.

## Nodes methods

```ts
question(question: string, options: TMachinistQuestionType|IMachinistQuestionOptions = null, blackboardKey: string = null): MachinistNode
```

Creates a new node that will contain a question that will be asked to the user.

- `question: string`: The text of the question to ask
- `options?: TMachinistQuestionType|IMachinistQuestionOptions`: The type or the options to use for this question (see [`TMachinistQuestionType` type](./t-machinist-question-type.md) and [`IMachinistQuestionOptions` interface](./i-machinist-question-options.md))
- `blackboardKey: string`: The name of the blackboard variable that will contain the answer to this question. If null, the answer won't be stored in the blackboard

Returns the created node.

Example:

```ts
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Create a question with default options, which is text type
machinist.question('What\'s your name?', null, 'name');
// Create a question that will be asked again until the answer is a number
machinist.question('How old are you?', 'integer', 'age');
// Create a question that will be asked again until the answer is one of the defined values
machinist.question('What\'s your gender? (female|male|other)', { type: 'enum', values: ['female', 'male', 'other']}, 'gender');
// Log the blackboard, which contain all the answers at the defined keys, here: { name, age, gender }
machinist.do((blackboard) => { console.log('Output blackboard', blackboard); });
// Run Machinist
machinist.run();
```

Since this method returns the new created node, chaining `question()` calls will cause to create sub-questions. If we take the previous example but chaining calls this time:

```ts
const machinist = require('@muffin-dev/machinist')();

machinist.question('What\'s your name?', null, 'name')
    .question('How old are you?', 'integer', 'age');
        .question('What\'s your gender? (female|male|other)', { type: 'enum', values: ['female', 'male', 'other']}, 'gender');

machinist.do((blackboard) => { console.log('Output blackboard', blackboard); });
machinist.run();
```

In this last example, the output will be the same. But you can compare the structures by using the `Machinist.logHierarchy()` methods. You will see that the second question is a child of the first, and the third is a child of the second one. It doesn't really matter in that example, but this behavior makes sense when it's combinated with conditions. See the `if()` and the `case()` methods for more informations about conditions.

Also see:

- [`IMachinistQuestionOptions` interface](./i-machinist-question-options.md)
- [`TMachinistQuestionType` type](./t-machinist-question-type.md)

---

```ts
setup(action: TMachinistAction): MachinistNode
```

Defines an action to execute just after this node's question have been asked, whatever its answer. Keep in mind that at this step, all the questions haven't been asked yet, and the blackboard could be incomplete.

- `action: TMachinistAction`: The action to execute after this node's question have been asked

Returns the current node itself. This also means that you can't chain `setup()` calls as you would do with the `do()` method. Only one *setup action* can be defined on each node.

---

```ts
do(action: TMachinistAction): MachinistNode
```

Creates an action to execute from this node.

- `action: TMachinistAction`: The action to execute

Returns the current node. Note that since it returns the current node itself, you can chain `do()` calls. This will result in adding several actions to perform on the same node.

Example:

```ts
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Creates a question
machinist.question('What\'s your name?')
    // Creates 2 actions to execute that uses that question answer
    .do((blackboard, answer) => { console.log(`Greet ${answer}`); })
    .do((blackboard, answer) => { console.log(`Show ${answer}'s agenda`)});
// Run Machinist
machinist.run();
```

Also see:

- [`TMachinistAction` type](./t-machinist-action.md)

---

```ts
if(condition: TMachinistCondition): MachinistNode
```

Creates a new node that will be executed only if the condition is fullfilled. It also means that all its child nodes are asked (in case of questions) and executed only if that condition is fullfilled.

- `condition: TMachinistCondition`: The method that will check for the condition

Returns the created node.

Example:

```js
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Create a question
machinist.question('What\'s your name?')
    // Only if the answer is null...
    .if((blackboard, answer) => { return answer === null; })
        // ... This question will be asked
        .question('Why don\'t you want to tell me your name?');
machinist.run();
```

Also see:

- [`TMachinistCondition` type](./t-machinist-condition.md)

---

```ts
case(expectedAnswer: any): MachinistNode
```

Creates a new node that will be executed only if the answer to the parent question macthes with the given `expectedAnswer`. It also means that all its child nodes are asked (in case of questions) and executed only if that condition is fullfilled.

The behaviour of this method is quite the same as the `if()` method, except it will only check for equality between the answer to a parent question, and the given `expectedAnswer`.

- `expectedAnswer`: The expected answer to this node's question. Note that the equality is strictly checked, so take care of the type of the question. For example, if a question is of type `integer`, checking for `1` can succeed, but checking for `"1"` will always fail

Returns the created node.

Example:

```js
// Create an instance of Machinist
const machinist = require('@muffin-dev/machinist')();
// Create a question
machinist.question('What\'s your name?')
    // Only if the answer is null...
    .case(null)
        // ... This question will be asked
        .question('Why don\'t you want to tell me your name?');
machinist.run();
```

## Navigation methods

These methods are meant to navigate through nodes when you create the tree.

```ts
get parent(): MachinistNode
```

Gets the parent node of this one. Note that it returns `null` if this is the root node.

---

```ts
get p(): MachinistNode
```

Alias for `parent()` accessor.

---

```ts
get parentQuestion(): MachinistNode
```

Goes up the hierarchy of this node to get the parent question node, and returns it. It returns `null` if there's no question node up in the hierarchy.

---

```ts
get pq(): MachinistNode
```

Alias for `parentQuestion()` accessor.

---

```ts
get back(levels: number): MachinistNode
```

Goes up the hierarchy by the given level. For example, `back(2)` will gets the parent of the parent node of this one. `back(1)` is equivalent to `parent()`.

- `levels: number`: The number of parent nodes to iterate through

Returns the found node.