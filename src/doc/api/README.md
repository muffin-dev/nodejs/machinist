# Muffin Dev for Node - Machinist - API Documentation

## Basics

### Import

Import *Machinist* in vanilla JS:

```js
// Import Machinist
const machinist = require('@muffin-dev/machinist');

// Create an instance of Machinist
const myMachinist = machinist();
```

---

Import *Machinist* using TypeScript:

```ts
// Import Machinist
import Machinist from '@muffin-dev/machinist';

// Create an instance of Machinist
const myMachinist = Machinist();
```

### Usage

```js
// Add a question to ask:
myMachinist.question('What\'s your name?')
    // Add an action to perform
    .do((blackboard, answer) => { console.log(`Hello ${answer}!`); });

// Run your Machinist instance
myMachinist.run();
```

## Classes

- [Machinist](./machinist.md): The top-level instance of a Machinist process
- [MachinistNode](./machinist-node.md): Part of the Machinist tree that can contain questions to ask, actions to execute, etc.

## Interfaces

- [IMachinistSettings](./i-machinist-settings.md): Represents the settings applicable on top-level Machinist instances
- [IMachinistQuestionOptions](./i-machinist-question-options.md): Represents options for a question to ask

## Types

- [TMachinistQuestionType](./t-machinist-question-type.md): Defines the type of a question
- [TMachinistBlackboard](./t-machinist-blackboard.md): Represents the blackboard object
- [TMachinistAction](./t-machinist-action.md): Represents an action to perform, generally at the "exec" phase
- [TMachinistCondition](./t-machinist-condition.md): Represents a method that will check for a condition