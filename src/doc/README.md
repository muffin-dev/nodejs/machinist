# Muffin Dev for Node - Machinist - Documentation

This documentation contains the API docs, and some tutorials.

For a quick start guide, see *Usage* and *Examples* section on [the main README file](../README.md).

## Summary

### API Documentation

- [`Machinist`](./api/machinist.md)
- [`MachinistNode`](./api/machinist-node.md)

### Tutorials

- [Make a Node project generator](./tutorials/project-generator.md)