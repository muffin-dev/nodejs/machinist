# Muffin Dev for Node - Machinist - Tutorials list

The following tutorials will show you how to use the *Machinist* library through the making of concrete projects.

For a quick start guide, see *Usage* and *Examples* section on [the main README file](../README.md).

## Tutorials list

- [Make a Node project generator](./tutorials/project-generator.md)