import Machinist from '../';

const machinist = Machinist({
  invalidAnswerMessage: 'Invalid answer, please try again...'
});

machinist
  .do(blackboard => {
    console.log('Notice project generation starts');
  })

machinist
  .if(blackboard => { return !blackboard.targetPath; })
  .question('Path to the project to generate:', { type: 'text', nullable: false }, 'targetPath');

machinist
  .do(blackboard => {
    console.log('Build project at path: ', blackboard.targetPath);
  })
  .do(() => {
    console.log('Add package.json to target project');
  })
  .do(() => {
    console.log('Add gitignore');
  })
  .do(() => {
    console.log('Add README');
  });

machinist.question('What kind of project do you want to generate? (app|package)',
  { type: 'enum', values: ['app', 'package'], nullable: false}, 'projectType')
  .case('project')
    .do(() => {
      console.log('Update package.json for package project');
    })
    .do(() => {
      console.log('Add build.js');
    })
    .do(() => {
      console.log('Add source scripts');
    })
  .pq.case('app')
    .do(() => {
      console.log('Update package.json for package project');
    })
    .do(() => {
      console.log('Add build.js');
    })
    .do(() => {
      console.log('Add source scripts');
    })
    .do(() => {
      console.log('Add server.js');
    })
    .do(() => {
      console.log('Install @muffin-dev/js-helpers & Express');
    });

machinist.question('Project name:', { nullable: false }, 'projectName');
machinist.question('Author (optional):', null, 'author');
machinist.question('Description (optional):', null, 'description');
machinist.question('Git repository URL (optional):', null, 'gitURL');

machinist.if(blackboard => { return blackboard.projectType === 'app'; })
  .question('Do you want to setup the app for heroku deployment? (Y/N)', null, 'setupHeroku')
    .do(() => {
        console.log('Update package.json');
      })
    .do(() => {
      console.log('Add Procfile');
    })
    .case(false)
      .question('Do you want to setup the app for Surge deployment? (Y/N)')
        .do(() => {
          console.log('Install Surge');
        })
        .do(() => {
          console.log('Update package.json');
        })
        .case(true)
          .question('Surge app domain (optional; example: my-app.surge.sh):')
            .case(true)
              .do(() => {
                console.log('Update package.json');
              })
              .do(() => {
                console.log('Add CNAME');
              })

machinist.if(blackboard => { return blackboard.projectType === 'package'; })
  .question('Does the package provide a bin program? (Y/N)', 'bool', 'packageBin')
    .case(true)
      .do(() => {
        console.log('Update package.json');
      })
      .do(() => {
        console.log('Add src/bin/index.js|ts');
      });

machinist.question('Do you want to use Typescript?', 'bool', 'useTypescript')
  .case(true)
    .do(() => {
      console.log('Update package.json');
    })
    .do(() => {
      console.log('Install Typescript');
    })
    .do(() => {
      console.log('Generate tsconfig.json');
    })
    .question('Do you want to use TSLint?', 'bool', 'useTSLint')
      .case(true)
        .do(() => {
          console.log('Update package.json');
        })
        .do(() => {
          console.log('Add tslint.json');
        })
        .do(() => {
          console.log('Install tslint');
        });

machinist
  .do(() => {
    console.log('Initialize git, first commit and set remote origin');
  })
  .do(blackboard => {
    console.log('Notice project generation ends', blackboard);
  });

(async () => {

  console.log('----- TEST START -----');
  console.log('');

  console.log('-- BEGIN LOG HIERARCHY --');
  machinist.logHierarchy(false);
  console.log('-- END LOG HIERARCHY --');
  console.log('');

  await machinist.run(null, { targetPath: './' });

  console.log('');
  console.log('----- TEST END -----');

})();