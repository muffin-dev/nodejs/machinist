# Muffin Dev for Node - Machinist

Machinist is a library for asking questions to a user (using a terminal or any other interface), getting user inputs, and executing tasks after all the questions have been asked.

This is for example very useful for making CLI tools.

## Installation

Install this package locally in your project by running:

```bash
npm i @muffin-dev/machinist
```

## Usage

Here is a simple usage of Machinist:

```js
const machinist = require('@muffin-dev/machinist');

// Create an instance of Machinist
const roomService = machinist();

// Add a question, which can only get integer number as an answer
roomService.question('Hello sir! What\'s your room number?', 'integer')
    // Define what to do with the given answer
    .do((blackboard, answer) => { console.log('Preparing breakfast for room n°' + answer); });

// Add a question, but this time, allow only boolean answer
roomService.question('Would you like to have some toasts with your breakfast? (Y/N)', 'boolean')
    // Create nodes to execute only if the answer to this question is true
    .case(true)
        .do(() => { console.log('Adding toasts'); });

// Use do() directly on the top-level Machinist instance to create an action to execute before or, in this case, after all the other actions have been executed
roomService.do(() => { console.log('Breakfast ready!'); });

// Run the Machinist instance, using the standard I/O by default
roomService.run();
```

## Machinist concepts

### The *ask* and *exec* phases

Machinist's process have two steps:

- An "ask" phase: all the questions you have defined are asked to the user
- An "exec" phase: all the actions you created are executed, depending on the eventual conditions you set

Asking the questions is a synchronous process: a question is asked, Machinist waits for user inputs, then it goes to the next question. The only exception is when you use `setup()`, which perform an action just after the question have been asked (more infos about it in the [completete documentation](./doc/api/machinist.md)).

Once all the questions have been asked, Machinist will execute all the actions. Note that you can use either synchronous and asynchronous methods when you use `do()` to define actions to perform.

### The *nodes* graph

See the Machinist structure as a tree graph. The top-level instance contains a *root node*, and when you use `question()` to create a new question, Machinist creates a new node, assigns the question to it, and returns the created node.

This is why you'll need to use `parent` or `parentQuestion` accessors in order to navigate through the nodes hierarchy, and manipulate the right node.

### Blackboard

The blackboard is an object used to store the answer to the previous questions. This is useful for several things:

- You can define an "input blackboard" when you use the `run()` method, which 

## Examples

Ask a simple question:

```js
const machinist = require('@muffin-dev/machinist');
const myMachinist = machinist();

myMachinist.question('Do you think it will work?');
myMachinist.run();
```

---

Define several actions to perform on the same question:

```js
const machinist = require('@muffin-dev/machinist');
const myMachinist = machinist();

// You can define an action with do(), and chain multiple calls to that method
myMachinist.question('What\'s your name?')
    .do((blackboard, answer) => { console.log(`Hi ${answer}!`); })
    .do(() => { console.log('*Waiting for the coffee to be ready*'); })
    .do(() => { console.log('Here is your coffee mate!'); });
myMachinist.run();
```

---

Use conditions and sub-questions:

```js
const machinist = require('@muffin-dev/machinist');
const myMachinist = machinist();

myMachinist.question('Do you like listening to music?', 'boolean')
    // case() is a shortcut for if(), based only on the answer of a parent question
    .case(true)
        // This question and all the child operations are executed only if the answer to the previous question is "true"
        .question('What\'s your favorite song?')
            .do((blackboard, answer) => { console.log('Play ' + answer); })
    // We need to navigate to the parent question using the parentQuestion accessor to define a new condition on the first question
    .parentQuestion.case(false)
        .question('Wait... What? But, do you have a soul?', 'boolean')
            .case(true)
                .do(() => { console.log('Ok, it reassures me!'); })
            .parentQuestion.case(false)
                .do(() => { console.log('This dude scares me...'); });

myMachinist.run();
```

---

Question with pre-defined answers (the *enum* question type):

```js
const machinist = require('@muffin-dev/machinist');
const myMachinist = machinist();

// This question will be asked again, until the answer is equal to one of the given "values" (please note that dogs are the best)
myMachinist.question('Do you prefer dogs or cats?', { type: 'enum', values: [ 'dogs', 'obviously dogs' ]})
    .do(() => { console.log('Yeah, I was already sure about that! ;)'); });

myMachinist.run();
```

---

Learn more about the available methods and the question options in the [full documentation](./doc/README.md)!

## Complete documentation

- [API Documentation](./doc/api/README.md)
- [Tutorials](./doc/tutorials/README.md)

## Future improvements

Add the `untilTrue()` method, that uses only boolean questions, and asked all of them until one is answered by `true`.

---

Add the *Show possible answers* Machinist instance option, which writes the possible answers to a question depending on its type.

---

Add `repeat()` method which allow you to repeat an entire node depending on a condition.

---

Add `else()` which will be executed only if other conditions of the current node (added with `if()` or `case()`) haven't been fullfilled.